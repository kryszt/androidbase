# What is it?

This is a basic, empty project with preconfigured most common dependencies in build file.

## Why is it?

To save us time on looking for build config during coding sessions on Face-to-Face interviews.
 
