# Open and closing parenthesis

Your task is to write a function that:
- takes in a string
- validates parenthesis in it
- returns true or false 

## Rules
- Each opening parenthesis needs to have a closing one (and vice-versa)  
- First parenthesis needs to be an opening one.

Example Inputs:
- "()"              => true
- "()()"            => true
- "(()"             => false
- "())"             => false
- "(hello)"         => true
- ") and then ("    => false

## Rules for 2nd round:
- Nested parenthesis are possible.

Example Inputs:
- "()))"            => false
- "())(()"          => false
- "(())((()())())"  => true
- "(())((()())()))" => false
